<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdvantageResource;
use App\Http\Resources\FranchisingDigitResource;
use App\Http\Resources\GalleryResource;
use App\Http\Resources\ProudDigitResource;
use App\Http\Resources\TariffResource;
use App\Models\Advantage;
use App\Models\FranchisingDigit;
use App\Models\Gallery;
use App\Models\ProudDigit;
use App\Models\Tariff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SiteController extends Controller
{
    public function index()
    {
        //Artisan::call('storage:link');
        $data = [];
        $data['tariffs'] = resourceData(TariffResource::collection(Tariff::where('active', 1)->orderBy('sort', 'ASC')->get()));
        $data['gallery'] = resourceData(GalleryResource::collection(Gallery::where('active', 1)->orderBy('sort', 'DESC')->get()));
        $data['advantages'] = resourceData(AdvantageResource::collection(Advantage::where('active', 1)->orderBy('sort', 'DESC')->get()));
        $data['proud_digits'] = resourceData(ProudDigitResource::collection(ProudDigit::where('active', 1)->orderBy('sort', 'DESC')->get()));
        $data['franchising_digits'] = resourceData(FranchisingDigitResource::collection(FranchisingDigit::where('active', 1)->orderBy('sort', 'DESC')->get()));
        return view('site.index', compact('data'));
    }
}
