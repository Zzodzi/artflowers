<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\GalleryResource;
use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $model = Gallery::where('active', 1)->with('attachment')->orderBy('sort', 'DESC')->get();
        //dd($model[0]->attachment[0]->relativeUrl);
        return GalleryResource::collection($model);
    }
}
