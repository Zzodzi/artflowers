<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Mail\ApplicationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApplicationController extends Controller
{
    public function sendApplication(ApplicationRequest $request)
    {
        $requestPost = $request->post();
        unset($requestPost['_token']);
        Mail::to(env('TO_EMAIL'))->send(new ApplicationMail($requestPost));
        $response['message'] = "Заявка успешно отправлена!";
        return $response;
    }
}
