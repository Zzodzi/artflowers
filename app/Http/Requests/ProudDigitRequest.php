<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProudDigitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'proud_digit.value' => 'required|max:255',
            'proud_digit.symbol' => 'max:255',
            'proud_digit.description' => 'required|max:255',
            'proud_digit.sort' => '',
            'proud_digit.active' => '',
        ];
    }
}
