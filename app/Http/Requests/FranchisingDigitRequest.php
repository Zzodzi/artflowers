<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FranchisingDigitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'franchising_digit.value' => 'required|max:255',
            'franchising_digit.symbol' => 'max:255',
            'franchising_digit.description' => 'required|max:255',
            'franchising_digit.sort' => '',
            'franchising_digit.active' => '',
        ];
    }
}
