<?
function resourceData($data) {
    return json_decode(json_encode($data));
}
function cleanPhone($phone){
    $chars = '!#+() ';
    return preg_replace('/['.$chars.']/', '', $phone);
}