<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Orchid\Screen\TD;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        TD::macro('bool', function () {

            $column = $this->column;
        
            $this->render(function ($datum) use ($column) {
                return view('orchid.bool',[
                    'bool' => $datum->$column
                ]);
            });
        
            return $this;
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
    
}
