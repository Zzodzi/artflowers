<?php

namespace App\Orchid\Resources;

use App\Models\Gallery;
use Orchid\Crud\Resource;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;

class GalleryResource extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Gallery::class;

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('name')
                ->title('Name'),
            Upload::make('attachment')
                ->groups('photos')->media(),
            // Upload::make('attachment')
            //     ->groups('photos2')->media(),
            Input::make('sort')
                ->title('Сортировка'),
            CheckBox::make('active')
                ->value(1)
                ->title('Active')
                ->sendTrueOrFalse()
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),
            TD::make('name'),
            TD::make('img')
                ->render(function (Gallery $gallery) {
                    $url = $gallery->attachment->isEmpty() ? '' : $gallery->attachment[0]->url;
                    return "<img style='height: 50px; border-radius: 5px' src='{$url}' alt='Image'>";
                }),
            TD::make('sort')->sort(),
            TD::make('active')->bool()
        ];
    }

    /**
     * Get the sights displayed by the resource.
     *
     * @return Sight[]
     */
    public function legend(): array
    {
        return [
            Sight::make('id'),
            Sight::make('name'),
            Sight::make('sort'),
            Sight::make('active')->render(function (Gallery $gallery) {
                return $gallery->active ? 'Активен' : 'Не активен';
            })
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    // public function with(): array
    // {
    //     return ['attachment'];
    // }

    public function onSave(ResourceRequest $request, Gallery $gallery)
    {
        $clearArray = $request->all();
        unset($clearArray['attachment']);

        $gallery->forceFill($clearArray)->save();
        $gallery->attachment()->sync(
            $request->input('attachment', [])
        );
    }

    public static function label(): string
    {
        return 'Галерея';
    }

    public static function description(): string
    {
        return '';
    }
}
