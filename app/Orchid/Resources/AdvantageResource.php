<?php

namespace App\Orchid\Resources;

use App\Models\Advantage;
use Orchid\Crud\Resource;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;

class AdvantageResource extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Advantage::class;

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('title')
                ->title('Title')->required(),
            TextArea::make('description')
                ->title('Description')->rows(5)->required(),
            Input::make('sort')
                ->title('Сортировка'),
            CheckBox::make('active')
                ->value(1)
                ->title('Active')
                ->sendTrueOrFalse()
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),
            TD::make('title'),
            //TD::make('description'),
            TD::make('sort')->sort(),
            TD::make('active')->bool(),
            // TD::make('active')->render(function (Advantage $advantage) {
            //     return $advantage->active ? 'Активен' : 'Не активен';
            // })->sort(),
        ];
    }

    /**
     * Get the sights displayed by the resource.
     *
     * @return Sight[]
     */
    public function legend(): array
    {
        return [
            Sight::make('id'),
            Sight::make('title'),
            Sight::make('description'),
            Sight::make('sort'),
            Sight::make('active')->render(function (Advantage $tariif) {
                return $tariif->active ? 'Активен' : 'Не активен';
            })
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    public static function label(): string
    {
        return 'Преимущества';
    }

    public static function description(): string
    {
        return 'Что вы получите, купив франшизу?';
    }
}
