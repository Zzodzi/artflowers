<?php

namespace App\Orchid\Resources;

use App\Models\Tariff;
use Orchid\Crud\Resource;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Sight;
use Orchid\Screen\TD;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Upload;

class TarrifsResource extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = Tariff::class;

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('name')
                ->title('Name')->required(),
            Upload::make('attachment')->maxFiles(1)
                ->groups('tariffs')->media(),
            Group::make([
                Input::make('costs')
                    ->title('Costs')->required(),
                Input::make('benefits')
                    ->title('Benefits')->required(),
                Input::make('arrived')
                    ->title('Arrived')->required(),
            ]),

            Input::make('sort')
                ->title('Сортировка'),
            CheckBox::make('active')
                ->value(1)
                ->title('Active')
                ->sendTrueOrFalse()

        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),
            TD::make('name'),
            TD::make('img')
                ->render(function (Tariff $tariif) {
                    $url = $tariif->attachment->isEmpty() ? '' : $tariif->attachment[0]->url;
                    return "<img style='height: 10px;' src='{$url}' alt='Image'>";
                }),
            TD::make('sort')->sort(),
            TD::make('active')->bool()
        ];
    }

    /**
     * Get the sights displayed by the resource.
     *
     * @return Sight[]
     */
    public function legend(): array
    {
        return [
            Sight::make('id'),
            Sight::make('name'),
            Sight::make('costs'),
            Sight::make('benefits'),
            Sight::make('arrived'),
            Sight::make('sort'),
            Sight::make('active')->render(function (Tariff $tariif) {
                return $tariif->active ? 'Активен' : 'Не активен';
            })
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    public function onSave(ResourceRequest $request, Tariff $tariff)
    {
        $clearArray = $request->all();
        unset($clearArray['attachment']);

        $tariff->forceFill($clearArray)->save();
        $tariff->attachment()->sync(
            $request->input('attachment', [])
        );
    }


    public static function label(): string
    {
        return 'Тарифы';
    }

    public static function description(): string
    {
        return '';
    }
}
