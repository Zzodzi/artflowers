<?php

namespace App\Orchid\Layouts\FranchisingDigit;

use App\Models\FranchisingDigit;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FranchisingDigitListTable extends Table
{
      /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'franchising_digits';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id'),
            TD::make('value'),
            TD::make('symbol'),
            TD::make('description'),
            TD::make('sort')->sort(),
            TD::make('active')->bool(),
            TD::make('created_at')->defaultHidden(),
            TD::make('updated_at')->defaultHidden(),
            TD::make('Действия')
                ->alignRight()
                ->render(function (FranchisingDigit $franchising_digit) {
                    return
                        ModalToggle::make('Редактировать')
                        ->icon('pencil')
                        ->modal('editFranchisingDigit')
                        ->method('createOrUpdate')
                        ->modalTitle('Редактировать цифру ' . $franchising_digit->id)
                        ->asyncParameters(['franchising_digit' => $franchising_digit->id])
                        .
                        Button::make('Удалить')
                        ->icon('ban')
                        ->confirm('After deleting, this will be gone forever.')
                        ->method('delete', ['franchising_digit' => $franchising_digit->id]);
                }),
        ];
    }
}
