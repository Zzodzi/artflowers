<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class CreateOrUpdateProudDigit extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('proud_digit.id')->type('hidden'),
            Input::make('proud_digit.value')
                ->title('Value')
                ->placeholder('Enter value')->required(),
            Input::make('proud_digit.symbol')
                ->title('Symbol')
                ->placeholder('Enter symbol'),
            Input::make('proud_digit.description')
                ->title('Description')
                ->placeholder('Enter description')->required(),
            Input::make('proud_digit.sort')->title('Сортировка'),
            CheckBox::make('proud_digit.active')
                ->value(1)
                ->title('Active')
                ->sendTrueOrFalse()
        ];
    }
}
