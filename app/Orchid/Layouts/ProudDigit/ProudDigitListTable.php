<?php

namespace App\Orchid\Layouts\ProudDigit;

use App\Models\ProudDigit;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ProudDigitListTable extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'proud_digits';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id'),
            TD::make('value'),
            TD::make('symbol'),
            TD::make('description'),
            TD::make('sort')->sort(),
            TD::make('active')->bool(),
            TD::make('created_at')->defaultHidden(),
            TD::make('updated_at')->defaultHidden(),
            TD::make('Действия')
                ->alignRight()
                ->render(function (ProudDigit $proud_digit) {
                    return
                        ModalToggle::make('Редактировать')
                        ->icon('pencil')
                        ->modal('editProudDigit')
                        ->method('createOrUpdate')
                        ->modalTitle('Редактировать цифру ' . $proud_digit->id)
                        ->asyncParameters(['proud_digit' => $proud_digit->id])
                        .
                        Button::make('Удалить')
                        ->icon('ban')
                        ->confirm('After deleting, this will be gone forever.')
                        ->method('delete', ['proud_digit' => $proud_digit->id]);
                }),
        ];
    }
}
