<?php

namespace App\Orchid\Screens;

use App\Http\Requests\ProudDigitRequest;
use App\Models\ProudDigit;
use App\Orchid\Layouts\CreateOrUpdateProudDigit;
use App\Orchid\Layouts\ProudDigit\ProudDigitListTable;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast as FacadesToast;

class ProudDigitScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'proud_digits' => ProudDigit::filters()->defaultSort('id', 'desc')->paginate(10),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Цифры';
    }

    /**
     * The description is displayed on the user's screen under the heading
     */
    public function description(): ?string
    {
        return 'Мы гордимся';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make('Add')
                ->modal('createProudDigit')
                ->method('createOrUpdate')
                ->icon('plus'),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            ProudDigitListTable::class,
            Layout::modal('createProudDigit', CreateOrUpdateProudDigit::class)
                ->title('Создать Proud Digit')
                ->applyButton('Создать'),

            Layout::modal('editProudDigit', CreateOrUpdateProudDigit::class)
                ->title('Обновить Proud Digit')
                ->applyButton('Обновить')
                ->async('asyncGetProudDigit')
        ];
    }

    public function asyncGetProudDigit(ProudDigit $proud_digit): array
    {
        return [
            'proud_digit' => $proud_digit
        ];
    }

    public function createOrUpdate(ProudDigitRequest $request): void
    {
        //dd($request->validated('proud_digit'));
        $proudDigitId = $request->input('proud_digit.id');
        ProudDigit::updateOrCreate([
            'id' => $proudDigitId
        ], $request->validated('proud_digit'));

        is_null($proudDigitId) ?  FacadesToast::info('Успешно создано') : FacadesToast::info('Успешно обновлено');
    }

    public function delete(ProudDigit $proud_digit): void
    {
        $proud_digit->delete();
        FacadesToast::info('Успешно удалено');
    }
}
