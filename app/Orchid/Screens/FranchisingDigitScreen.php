<?php

namespace App\Orchid\Screens;

use App\Http\Requests\FranchisingDigitRequest;
use App\Models\FranchisingDigit;
use App\Orchid\Layouts\CreateOrUpdateFranchisingDigit;
use App\Orchid\Layouts\FranchisingDigit\FranchisingDigitListTable;
use Orchid\Screen\Screen;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FranchisingDigitScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'franchising_digits' => FranchisingDigit::filters()->defaultSort('id', 'desc')->paginate(10),
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Цифры';
    }

    public function description(): ?string
    {
        return 'Франчайзинг';
    }


    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make('Add')
                ->modal('createFranchisingDigit')
                ->method('createOrUpdate')
                ->icon('plus'),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            FranchisingDigitListTable::class,
            Layout::modal('createFranchisingDigit', CreateOrUpdateFranchisingDigit::class)
                ->title('Создать Franchising Digit')
                ->applyButton('Создать'),

            Layout::modal('editFranchisingDigit', CreateOrUpdateFranchisingDigit::class)
                ->title('Обновить franchising Digit')
                ->applyButton('Обновить')
                ->async('asyncGetfranchisingDigit')
        ];
    }

    public function asyncGetFranchisingDigit(FranchisingDigit $franchising_digit): array
    {
        return [
            'franchising_digit' => $franchising_digit
        ];
    }

    public function createOrUpdate(FranchisingDigitRequest $request): void
    {
        $franchisingDigitId = $request->input('franchising_digit.id');
        FranchisingDigit::updateOrCreate([
            'id' => $franchisingDigitId
        ], $request->validated('franchising_digit'));

        is_null($franchisingDigitId) ? Toast::info('Успешно создано') : Toast::info('Успешно обновлено');
    }

    public function delete(FranchisingDigit $franchising_digit): void
    {
        $franchising_digit->delete();
        Toast::info('Успешно удалено');
    }

}
