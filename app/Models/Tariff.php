<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Tariff extends Model
{
    use AsSource, Filterable, Attachable;

    protected $allowedSorts = [
        'active',
        'sort'
    ];

    protected $fillable = [
        'name',
        'img',
        'costs',
        'benefits',
        'arrived',
        'active',
        'sort'
    ];

    protected $casts = [
        'img' => 'array',
    ];

    public function imgAttachment()
    {
        return $this->hasOne(Attachment::class, 'id', 'img')->withDefault();
    }
}
