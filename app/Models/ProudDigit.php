<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;

class ProudDigit extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $allowedSorts = [
        'active',
        'sort'
    ];

    protected $fillable = [
        'value',
        'description',
        'symbol',
        'active',
        'sort'
    ];
}
