<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class FranchisingDigit extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $allowedSorts = [
        'active',
        'sort'
    ];

    protected $fillable = [
        'value',
        'description',
        'symbol',
        'active',
        'sort'
    ];
}
