<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Advantage extends Model
{
    use AsSource, Filterable, Attachable;

    protected $allowedSorts = [
        'active',
        'sort'
    ];

    protected $fillable = [
        'title',
        'description',
        'active',
        'sort'
    ];
}
