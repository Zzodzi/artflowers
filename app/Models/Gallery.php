<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Gallery extends Model
{
    use AsSource, Filterable, Attachable;

    protected $allowedSorts = [
        'active',
        'sort'
    ];

    protected $fillable = [
        'name',
        'active',
        'sort'
    ];

}
