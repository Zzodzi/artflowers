<?php
function resourceData($data) {
    return json_decode(json_encode($data));
}
function cleanPhone($phone){
    $chars = '!#+() ';
    return preg_replace('/['.$chars.']/', '', $phone);
}

function tmbPath($img){
    return str_replace('uploads/', 'uploads/.tmb/', $img);
}
