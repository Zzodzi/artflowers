<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\front\SiteController;
use App\Http\Controllers\StorageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(SiteController::class)->group(function () {
    Route::get('/', 'index');
});

Route::controller(ApplicationController::class)->group(function () {
    Route::post('/application/send-application', 'sendApplication');
});

