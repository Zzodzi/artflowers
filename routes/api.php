<?php

use App\Http\Controllers\Api\AdvantageController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\ProudDigitController;
use App\Http\Controllers\Api\TariffController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::apiResources([
//     'advantages' => AdvantageController::class,
//     'tariffs' => TariffController::class,
//     'galleries' => GalleryController::class,
//     'proud_digits' => ProudDigitController::class,
// ]);
