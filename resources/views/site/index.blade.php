@extends('layouts.master')

@section('content')
    <section id="section-top">
        <div class="__bg"></div>
        <header>
            <div class="container">
                <a class="a-logo" href="/"><img src="/img/logo-white.svg" alt=""></a>
                <nav>
                    <ul>
                        <li><a href="#unique">Кто мы</a></li>
                        <li><a href="#franchising">Франшиза</a></li>
                        <li><a href="#questions">Контакты</a></li>
                    </ul>
                </nav>
                <div class="burger-menu"><span></span><span></span><span></span></div>
            </div>
        </header>
        <div class="container">
            <div class="__left">
                <h1>Франшиза от <span>крупнейшего импортера</span> цветов в Казахстане</h1>
                <p>Полностью прозрачная авторская структура бизнеса и 100% поддержка на всех этапах запуска </p>
            </div>
            <div class="__right">
                <img class="video" src="/img/big-flower.png" alt="">
                <div class="bottom">
                    <a class="start-business" data-to-overlay="leave-request" data-formtype="Узнать как начать бизнес">
                        <span>узнать, как начать бизнес</span>
                    </a>
                    <p>Откройте франшизу сейчас и начинайте зарабатывать уже со второго месяца!</p>
                </div>
            </div>
        </div>
    </section>
    <section id="unique">
        <div class="container __top">
            <h2 data-scroll data-scroll-speed="3" data-scroll-direction="horizontal">Уникальная
                <span>бизнес-модель</span>
            </h2>
        </div>
        <div class="wrapper">
            <div class="outer-flower bus-flower-1">
                <img class="petal" src="/img/bus-flower-1.png" alt="">
            </div>
            <div class="outer-flower bus-flower-2">
                <img class="petal" src="/img/bus-flower-2.png" alt="">
            </div>

            <div class="container">
                <div class="__bg"></div>
                <div class="see-video-outer" data-to-overlay="watch-video">
                    <div class="circle">
                        <svg width="15.874023" height="17.168945" viewBox="0 0 15.874 17.1689" fill="none"
                            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <defs />
                            <path id="Polygon 1"
                                d="M14.8438 6.83521L2.96973 0.253662C1.63672 -0.485107 0 0.479004 0 2.00293L0 15.166C0 16.6899 1.63672 17.6541 2.96973 16.9153L14.8438 10.3337C16.2178 9.57227 16.2178 7.59668 14.8438 6.83521Z"
                                fill="#FCFCFC" fill-opacity="1.000000" fill-rule="evenodd" />
                        </svg>
                    </div>
                    <span>смотреть шоурил</span>
                </div>
                <div class="quotes-outer">
                    <img src="/img/quotes.svg" alt="">
                    <p>Красивый бизнес с надежным партнером – это франшиза ArtFlowers. <br><br>Динара Сатжан</p>
                </div>

                <div class="info">
                    <h3>Мы упаковали наш 15 летний опыт во франшизу и гарантируем ее успех!</h3>
                    <p>Если хотите открыть цветочный бренд №1 в своем городе и стать частью большой компании, welcome!</p>
                </div>
                <img class="woman" src="/img/woman.png" alt="">
            </div>
        </div>
    </section>
    <section id="proud">
        <div class="container">
            <div class="row">
                <div class="__left">
                    @foreach ($data['proud_digits'] as $digit)
                        <div class="digit-outer">
                            <div class="top number-wrapper">
                                <span class="number" data-goal="{{ $digit->value }}">0</span><span
                                    class="symbol">{{ $digit->symbol }}</span>
                            </div>
                            <span>{{ $digit->description }}</span>
                        </div>
                    @endforeach
                </div>
                <div class="__right">
                    <h2>Можем этим <span>гордиться</span></h2>
                </div>
                <img data-scroll class="petal petal-1" src="/img/petal-1.png" alt="">
            </div>
        </div>
    </section>
    <section id="gallery">
        <img class="gallery-top" src="/img/gallery-top.jpg" alt="">
        <div class="swiper" id="swiperGallery">
            <div class="swiper-wrapper">
                @foreach ($data['gallery'] as $item)
                    @foreach ($item->photos as $img)
                        <div class="swiper-slide"><img src="{{ $img->url }}" alt=""></div>
                    @endforeach
                    @foreach ($item->photos as $img)
                        <div class="swiper-slide"><img src="{{ $img->url }}" alt=""></div>
                    @endforeach
                @endforeach
            </div>
        </div>
        <img class="gallery-bottom" src="/img/gallery-bottom.jpg" alt="">
    </section>
    <section id="more-unique">
        <div class="container">
            <div class="__left">
                <h4 data-scroll data-animation-top>А в чем, собственно, уникальность?</h4>
                <p data-scroll data-animation-top style="transition-delay: .1s">
                    Для нас важна этика бизнеса и мы гордимся своим опытом централизованных закупок цветов, что
                    позволяет нам давать партнерам и клиентам лучшие предложения со всего мира.<br><br>
                    А так же наш 15 летний опыт помогает всегда улучшать качество нашей цветочной продукции и
                    совершенствовать процессы поставок.<br><br>
                    Вы получаете готовую оцифрованную модель цветочного бизнеса, от закупа до продаж
                </p>
            </div>
            <div class="__right">
                <img class="truck-bg" src="/img/truck-bg.svg" alt="" data-scroll data-animation-top>
                <img class="truck" src="/img/truck.png" alt="" data-scroll data-animation-top>
            </div>
        </div>
    </section>
    <section id="what-u-get">
        <div class="container">
            <div class="row">
                <img class="what-u-get" src="/img/what-u-get.png" alt="">
                <div class="col-100">
                    <h3 data-scroll data-animation-top>Что вы получите, купив франшизу?</h3>
                </div>
                @foreach ($data['advantages'] as $advantage)
                    <div class="col-50">
                        <div class="card" data-scroll data-animation-top>
                            <h4>{{ $advantage->title }}</h4>
                            <p>
                                {{ $advantage->description }}
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section id="franchising">
        <div class="container">
            <div class="row">
                <div class="col-50 __left">
                    <h4 data-scroll data-animation-top>
                        Франчайзинг и партнерство
                    </h4>
                    <p data-scroll data-animation-top style="transition-delay: .1s">
                        Мы научим вас всем фишкам, передадим технологию и готовую бизнес - модель для Вашего
                        города.<br><br>
                        Только работающие инструменты, опыт и практика!
                    </p>
                    <div class="be-part" data-to-overlay="leave-request" data-formtype="Стать частью команды">
                        <img class="little-petal-bg" src="/img/little-petal-bg.svg" alt="">
                        <span data-to-overlay="leave-request" data-formType="Стать частью команды">Стать частью
                            команды</span>
                    </div>
                </div>
                <div class="col-50 __right">
                    @foreach ($data['franchising_digits'] as $digit)
                        <div class="digit-outer">
                            <div class="top number-wrapper">
                                <span class="number" data-goal="{{ $digit->value }}">0</span><span
                                    class="symbol">{{ $digit->symbol }}</span>
                            </div>
                            <span>{{ $digit->description }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section id="open-business">
        <div class="container">
            <h4 data-scroll data-animation-top>Откройте свой бизнес всего за 50 дней, воспользовавшись одним из 3х простых доступных пакетов</h4>
            <div class="row">
                @foreach ($data['tariffs'] as $tariff)
                    <div class="col-33">
                        <div class="tariff" data-scroll data-animation-top>
                            <span>{{ $tariff->name }}</span>
                            <img class="rate" src="{{ $tariff->img->relativeUrl }}" alt="">
                            <ul class="info">
                                <li>{{ $tariff->costs }} затрат</li>
                                <li>{{ $tariff->benefits }} выгод</li>
                                <li>{{ $tariff->arrived }} прибыли</li>
                            </ul>
                            <div class="btn" data-to-overlay="leave-request"
                                data-formType="тариф {{ $tariff->name }}">Подробнее</div>
                        </div>
                    </div>
                @endforeach
                <img data-scroll class="petal petal-2" src="/img/petal-2.png" alt="">
            </div>
        </div>
    </section>
    <section id="brand-history">
        <div class="container">
            <h2 data-scroll data-animation-top>История <span>бренда</span></h2>
            <span data-scroll data-animation-top style="transition-delay: .1s" class="top-text">
                ArtFlowers начал свою историю в 2009 году с амбициозной миссии:<br>
                поставлять людям свежесть и красоту в виде большого ассортимента качественных цветов.
            </span>
            <p data-scroll data-animation-top style="transition-delay: .2s">
                С годами наша компания стала ключевым игроком в цветочной индустрии, устанавливая прочные
                партнерства с крупнейшими поставщиками цветов в Голландии, Эквадоре, Кении и других странах. Эти
                партнерства обеспечивают нам доступ к широкому ассортименту высококачественных цветов и
                растений.<br><br>
                За 15 лет мы смогли выстроить все процессы: закупки, хранение, реализация, маркетинг, доставка.
                Благодаря этому у нас не только есть индивидуальная работающая модель, но и понимание сложностей
                начинающих франчайзи. Поэтому мы можем помочь партнерам не только войти в нишу в своем регионе,
                но и избежать ошибок и сложностей, через которые прошли сами.
                <br><br><br>
                Артем Бугаев, основатель бренда Art Flowers
            </p>
        </div>
    </section>
    <section id="questions">
        <div class="container">
            <div class="wrapper">
                <h2 data-scroll data-animation-top>У Вас остались вопросы? <br>Задайте их нам!</h2>
                <form action="/application/send-application" data-scroll data-animation-top style="transition-delay: .1s"
                    class="row __default __ajax" method="post">
                    <div class="col-50">
                        @csrf
                        <label>
                            <span>Имя: </span>
                            <input name="name" type="text" placeholder="Ваше имя" required>
                        </label>
                    </div>
                    <div class="col-50">
                        <label>
                            <span>Email: </span>
                            <input name="email" type="email" placeholder="Ваш email" required>
                        </label>
                    </div>
                    <div class="col-50">
                        <label>
                            <span>Телефон</span>
                            <input name="phone" class="phoneInput" type="text" placeholder="+7-___-___-__-__"
                                required>
                        </label>
                    </div>
                    <div class="col-50">
                        <label>
                            <span>Город:</span>
                            <input name="city" type="text" placeholder="Город открытия магазина" required>
                        </label>
                    </div>
                    <div class="col-50">
                        <button class="btn __blue" type="submit">Оставить заявку</button>
                        <p class="notification">Нажимая кнопку вы соглашаетесь на обработку <span
                                data-to-overlay="personal-agreed">персональных данных.</span></p>
                    </div>
                    <input type="hidden" name="formType" value="Заяка" required>
                </form>
            </div>
        </div>
    </section>
@endsection
