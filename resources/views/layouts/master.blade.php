<!DOCTYPE html>
@php $lang = App::getLocale() @endphp
<html lang="{{ $lang }}">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="Artflowers">
    <meta name="keywords" content="Artflowers">
    <link rel="icon" type="image/x-icon" href="/img/logo.svg">
    @vite(['resources/css/main.sass', 'resources/js/main.ts'])
    <title>Artflowers</title>
</head>

<body>
    <div id="content" class="smooth-scroll" data-scroll-container>
        @yield('content')
        <footer>
            <div class="container">
                <div class="top">
                    <a class="a-logo" href="/"><img src="/img/logo-white.svg" alt=""></a>
                    <div class="__right">
                        <h4>Контакты:</h4>
                        <ul>
                            <li><a href="tel: +77719499009">+7 771 949 90 09</a></li>
                            <li>г. Алматы, ул. Тимирязева, 42к10а</li>
                            <li><a href="mailto: info@artflowers.kz">info@artflowers.kz</a></li>
                        </ul>
                        <div class="socials">
                            <a href="https://t.me/ArtFlowerss" target="_blank"><img src="/img/icon-tg.svg" alt=""></a>
                            <a href="https://www.facebook.com/people/Art-Flowers-Almaty/100034425193005/?locale=ru_RU" target="_blank"><img
                                    src="/img/icon-fb.svg" alt=""></a>
                            <a href="https://www.instagram.com/artflowerskz" target="_blank"><img src="/img/icon-instagram.svg" alt=""></a>
                            <a href="https://wa.me/87775225375" target="_blank"><img src="/img/icon-wp.svg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="bottom">
                    <span>All rights reserved © {{ date('Y') }} Artflowers</span>
                    <span data-to-overlay="personal-agreed">Политика конфиденциальности</span>
                    <span>Powered by <a href="https://brainteam.kz" target="_blank">BrainTeam.kz</a> ©
                        {{ date('Y') }}</span>
                </div>
            </div>
        </footer>
    </div>
    <div class="overlay personal-agreed">
        <div class="modal">
            <img class="cross" src="/img/cross.svg" alt="">
            <h2>СОГЛАШЕНИЕ ОБ ОБРАБОТКЕ ПЕРСОНАЛЬНЫХ ДАННЫХ</h2>
            <p>Данное соглашение об обработке персональных данных разработано в соответствии с законодательством РК.
                Все лица, заполнившие сведения, составляющие персональные данные на данном сайте, а также разместившие
                иную информацию обозначенными действиями, подтверждают свое согласие на обработку персональных данных и
                их передачу оператору обработки персональных данных.
                <br><br>
                Под персональными данными Гражданина понимается нижеуказанная информация: Общая информация (Имя, телефон
                и адрес электронной почты); посетители сайта направляют свои персональные данные для получения подробной
                информации о услугах.
                <br><br>
                Гражданин, принимая настоящее Соглашение, выражает свою заинтересованность и полное согласие, что
                обработка его персональных данных может включать в себя следующие действия: сбор, систематизацию,
                накопление, хранение, уточнение (обновление, изменение), использование, уничтожение.
                <br><br>
                Гражданин гарантирует: информация , им предоставленная, является полной, точной и достоверной; при
                предоставлении информации не нарушается действующее законодательство РК, законные права и интересы
                третьих лиц; вся предоставленная информация заполнена Гражданина в отношении себя лично.
            </p>
        </div>
    </div>
    <div class="overlay leave-request">
        <div class="modal">
            <img class="cross" src="/img/cross.svg" alt="">
            <h2>Оставить заявку</h2>
            <form action="/application/send-application" class="__default __ajax" method="post">
                @csrf
                <label>
                    <span>Имя: </span>
                    <input name="name" type="text" placeholder="Ваше имя" required>
                </label>
                <label>
                    <span>Телефон</span>
                    <input name="phone" class="phoneInput" type="text" placeholder="+7-___-___-__-__" required>
                </label>
                <label>
                    <span>Город:</span>
                    <input name="city" type="text" placeholder="Город открытия магазина" required>
                </label>
                <input type="hidden" name="formType" value="Заяка" required>
                <button class="btn __blue" type="submit">Оставить заявку</button>
                <p class="notification">Нажимая кнопку вы соглашаетесь на обработку <span
                        data-to-overlay="personal-agreed">персональных данных.</span></p>
            </form>
        </div>
    </div>
    <div class="overlay watch-video">
        <img class="cross" src="/img/cross.svg" alt="">
        <iframe class="video" width="90%" height="auto"
            src="https://www.youtube.com/embed/dK2FJQX24Io?si=oYkiCuCwXxq974B6" title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen></iframe>
    </div>
    @handheld
        <div class="mobile-menu">
            <div class="container">
                <div class="__top">
                    <a class="a-logo" href="/"><img src="/img/logo-white.svg" alt=""></a>
                    <img class="cross" src="/img/cross.svg" alt="">
                </div>
                <nav class="menu-nav">
                    <ul>
                        <li><a href="#unique">Кто мы</a></li>
                        <li><a href="#franchising">Франшиза</a></li>
                        <li><a href="#questions">Контакты</a></li>
                    </ul>
                </nav>
                <div class="socials">
                    <a href="https://t.me/ArtFlowerss" target="_blank"><img src="/img/icon-tg.svg" alt=""></a>
                    <a href="https://www.facebook.com/people/Art-Flowers-Almaty/100034425193005/?locale=ru_RU" target="_blank"><img
                            src="/img/icon-fb.svg" alt=""></a>
                    <a href="https://www.instagram.com/artflowerskz" target="_blank"><img src="/img/icon-instagram.svg" alt=""></a>
                    <a href="https://wa.me/87775225375" target="_blank"><img src="/img/icon-wp.svg" alt=""></a>
                </div>
            </div>
        </div>
    @endhandheld
</body>

</html>
