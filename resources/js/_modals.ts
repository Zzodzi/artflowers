import $ from "jquery"

//modals
export const modalEvents = () => {
    $('[data-to-overlay]').click(function () {
        const formType = $(this).attr('data-formType')
        if (formType) {
            $(".overlay.leave-request  input[name='formType']").val(formType)
        }
        const overlayName = $(this).attr('data-to-overlay')
        $('.' + overlayName).css("display", "flex")
            .hide()
            .fadeIn()
        if (overlayName == 'watch-video') {
            $(`.${overlayName} .video`)[0].src += "&autoplay=1"
        }
    })

    $('.modal .cross').click(function () {
        $(this).parent().parent().fadeOut()
    })

    $('.overlay').click(function () {
        $(this).fadeOut()
        const src = $(this).find('.video')[0].src
        $(this).find('.video')[0].src = src.replace('&autoplay=1', '')
    })

    $('.modal').click(function (e) {
        e.stopPropagation()
    })

    /*Mobile Menu*/
    $('.burger-menu').click(function () {
        $('.mobile-menu').addClass('opened')
    })

    $('.mobile-menu .cross').click(function () {
        $('.mobile-menu').removeClass('opened')
    })
}
