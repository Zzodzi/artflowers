import $ from "jquery";
import gsap from "gsap";

//locomotive
import LocomotiveScroll from 'locomotive-scroll';
import ScrollTrigger from "gsap/ScrollTrigger";
import ScrollToPlugin from "gsap/ScrollToPlugin";

let mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
let uniqueZoneBgDivider = mobile ? 210 : 390

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin)

const scroller = document.querySelector('.smooth-scroll');
export const locoScroll = new LocomotiveScroll({
    el: scroller,
    smooth: true
});
locoScroll.on("scroll", ScrollTrigger.update);

ScrollTrigger.scrollerProxy(scroller, {
    scrollTop(value) {
        return arguments.length ? locoScroll.scrollTo(value, { duration: 0, disableLerp: true }) : locoScroll.scroll.instance.scroll.y;
    },
    getBoundingClientRect() {
        return { top: 0, left: 0, width: window.innerWidth, height: window.innerHeight };
    },
    // @ts-ignore
    pinType: scroller.style.transform ? "transform" : "fixed"
});
ScrollTrigger.defaults({
    scroller: scroller
})
export const scrollTrigger = ScrollTrigger

//Navigation Events
$('nav a, .anchor').click(function () {
    const targetId = this.getAttribute('href')
    locoScroll.scrollTo(targetId);
})

$('.mobile-menu nav a').click(function () {
    $('.mobile-menu').removeClass('opened')
})


//GSAP ScrollTriggers
const animateProudNumbers = () => {
    const numberElements = document.querySelectorAll('section#proud .number');
    $('section#proud .number-wrapper').addClass('scrolled')
    numberElements.forEach(function (element) {
        const targetNumber = element.dataset.goal
        gsap.to(element, {
            duration: 2,
            innerHTML: targetNumber,
            roundProps: "innerHTML",
            ease: 'power2.out',
            onUpdateParams: ["{self}"]
        });
    });
}
ScrollTrigger.create({
    trigger: "section#proud",
    start: "top center",
    onEnter: animateProudNumbers,
})


const animateFranchisingNumbers = () => {
    const numberElements = document.querySelectorAll('section#franchising .number');
    $('section#franchising .number-wrapper').addClass('scrolled')
    numberElements.forEach(function (element) {
        const targetNumber = element.dataset.goal
        gsap.to(element, {
            duration: 2,
            innerHTML: targetNumber,
            roundProps: "innerHTML",
            ease: 'power2.out',
            onUpdateParams: ["{self}"]
        });
    });
}
ScrollTrigger.create({
    trigger: "section#franchising",
    start: "top center",
    onEnter: animateFranchisingNumbers,
})

const enterUniqueZone = () => {
    const uniqueWrapperBg = $('section#unique .wrapper .__bg')
    uniqueWrapperBg.css("transform", `scale(${window.innerWidth / uniqueZoneBgDivider})`)
    $('section#unique .wrapper').addClass('scrolled')
}

ScrollTrigger.create({
    trigger: "section#unique .wrapper",
    start: "top center",
    onEnter: enterUniqueZone,
})

