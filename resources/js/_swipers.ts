// import Swiper JS
import Swiper from 'swiper';
import { Navigation, Pagination, Autoplay } from 'swiper/modules';
Swiper.use([Navigation, Pagination, Autoplay]);

// import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

export const swiperGallery = new Swiper('#swiperGallery', {
    slidesPerView: 3.7,
    spaceBetween: 22,
    speed: 6000,
    loop: true,
    autoplay: {
        delay: 1,
        pauseOnMouseEnter: true
    },
});