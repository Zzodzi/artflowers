import $ from "jquery"
import { swiperGallery } from './_swipers'
import { locoScroll, scrollTrigger } from './_locomotive_scrolltrigger'
import IMask from 'imask';
import { modalEvents } from './_modals'

let mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
let topBgDivider = mobile ? 38 : 187

//Modals
modalEvents()

//Imask
const phoneInputs = document.querySelectorAll('.phoneInput')
phoneInputs.forEach(function (input) {
    IMask(input, {
        mask: '+{7}-000-000-00-00'
    })
});

//Swipers
swiperGallery

//animate-top-section
const openTopSection = () => {
    const topBg = $('section#section-top .__bg')
    topBg.css("transform", `scale(${window.innerWidth / topBgDivider})`)
    $('section#section-top .video').css('opacity', 1)
    $('section#section-top .video').css('transform', 'scale(1) rotate(0deg)')
}

window.onload = function () {
    $(".my-intro").addClass('close')
    $('html').addClass('loaded')
    openTopSection()
    locoScroll.update()
    scrollTrigger.refresh()
}

$(window).resize(function () {
    openTopSection()
})

//animate-petal-cursor
var petals = $('.petal');
var mouseX = 0
var mouseY = 0

let petalX = 0
let petalY = 0
let speed = 0.09

function movePetals() {
    let distX = mouseX - petalX
    let distY = mouseY - petalY

    petalX = petalX + (distX * speed)
    petalY = petalY + (distY * speed)

    $(petals).each(function () {
        let petal = this
        $(petal).css("transform", `translate(${petalX / 15}px, ${petalY / 15}px) rotate(${petalX / 130}deg)`)
    })

    requestAnimationFrame(movePetals);
}

movePetals()

$(document).mousemove(function (event) {
    mouseX = event.pageX;
    mouseY = event.pageY;
});

//Forms
$('form.__ajax').submit(function(e){
    e.preventDefault();
    let btn = $(this).find("button[type='submit']");
    let link = $(this).attr('action');
    let formData = $(this).serialize();
    
    btn.prop('disabled', true);
    btn.attr('disabled');
    btn.text('Отправка...');
    $.ajax({
        url: link,
        type: 'POST',
        dataType: 'json',
        data: formData,
        success: function($response) {
            btn.prop('disabled', false);
            btn.removeAttr('disabled');
            btn.text('Оставить заявку');
            alert($response.message)
        },
        error: function(error) {
            console.error('Error:', error);
            btn.prop('disabled', false);
            btn.removeAttr('disabled');
            btn.text('Оставить заявку');
        }
    });
});